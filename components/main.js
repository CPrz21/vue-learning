Vue.component('task-list',{
  template: `
            <div>
              <task v-for="task in tasks" v-text="task.task"></task>
            </div>
            `,
  data(){
    return {
      tasks: [
        { task:'Go to store', completed:true },
        { task:'Finish Class', completed:false },
        { task:'Clear', completed:false },
        { task:'Go to chapu', completed:true },
        { task:'Go to Elaniin', completed:true },
        { task:'Go to work', completed:true },
      ]
    }
  }
});

Vue.component('task', {
  template: '<li><slot></slot></li>'
});

new Vue({
  el:'#root'
});